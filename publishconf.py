import os
import sys

sys.path.append(os.curdir)
from pelicanconf import *

SITEURL = 'https://marsbot.gitlab.io/pelican-base'
RELATIVE_URLS = False
DELETE_OUTPUT_DIRECTORY = True

SMARTLOOK = True
