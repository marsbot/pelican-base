'use strict';

var menu = (function() {

    var menuToggle = document.getElementById('menu-toggle');
    var sideMenu = document.getElementById('side-menu');
    var closeMenu = document.getElementById('menu-close');

    function init() {
        menuToggle.addEventListener('click', function() {
            sideMenu.classList.add('sideMenuToggle');
            menuToggle.style.visibility = 'hidden';
        });

        closeMenu.addEventListener('click', function() {
            sideMenu.classList.remove('sideMenuToggle');
            menuToggle.style.visibility = 'visible';
        });
    }

    return {
        init: init
    }
})();
